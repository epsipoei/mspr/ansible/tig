<div align="center">
<img src="docs/media/tig-logo.png" width="300"/>
</div>


<br>
</br>

___

# Telegraf Influx Grafana

## **Summary**

[[*TOC*]]

### Installation automatisé de la stack monitoring TIG
___

- #### **Lab Virtualbox**
___

Sur virtualbox créer l'interface réseau

<img src="docs/media/network-virtualbox.png " width="600"/>

- #### **Installer Ansible**

    ```bash
    apt install ansible
    ```

- #### **Installer Vagrant**

    - https://developer.hashicorp.com/vagrant/install


    - Plugins
        - VirtualBox:
            ```bash
            vagrant plugin install vagrant-vbguest
            ```


- #### **Personnaliser le fichier vagrant**
___

- **sur virtualbox**
```bash
vagrant/virtualbox/vagrantfile
```
vous pourrez y definir l'ip, les ressources... ainsi que le path du script si vous souhaitez définir votre propre script.  
*par default l'ip utilisé pour le noeuds commence a 192.168.56.1# +i*

- #### **Personnaliser le fichier all.yml et hosts**
___

```bash
playbook/group_vars/all.yml
inventory/hosts
```
Lors du **"make run"** vous serez invité à définir les variables qui rempliront **automatiquement** le fichier **"all.yml"**.  
**Par contre le fichier hosts doit etre défini avant.**  
**ATTENTION !!** le nommage des hosts dois correspondre avec le nom des assets.

- #### **Install & Run with Make**
___

Personnaliser le fichier **Makefile** pour definir l'utilisateur et ip des serveur pour la partie ssh
```bash
ssh-copy-id -i ~/.ssh/id_ed25519.pub user@ip-srv
```

- **Install make**
    ```bash
    apt install make -y
    ```
- **Créer les vms**
    ```bash
    make lab-virtualbox
    ```
- **Executer Ansible**
    ```bash
    make run
    ```
- **Clean le Lab**
    ```bash
    make clean
    ```
___
- Notes:
___
Mise en place de telegraf, influxdb, grafana:

- <https://antoinelounis.com/informatique/supervision/installation-grafana-influxdb-telegraf-debian/>
- <https://grafana.com/grafana/dashboards/12433-syslog/>

Grafana dashboard id:

- rsyslog: 12433
- metric: 928

Pour reset le mp admin en cas d'oubli :)

```bash
sudo grafana-cli admin reset-admin-password admin
```