#!/bin/bash

# Mise à jour des dépôts
sudo apt-get update
sudo apt-get install -y sudo vim

# Ajout d'un nouvel utilisateur et définition du mot de passe
sudo adduser --disabled-password --gecos "" debian
echo 'debian:iopiop' | sudo chpasswd

# Assurez-vous que l'utilisateur dispose des privilèges sudo
sudo usermod -aG sudo debian

# Utilisation de sed pour modifier le fichier de configuration
sudo sed -i 's/^PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config

# Redémarrage du service SSH pour appliquer les modifications
sudo service ssh restart